<?php

/**
 * @file
 * Implements ActiveTrail as a Drupal MailSystemInterface
 */

/**
 * Modify the drupal mail system to use ActiveTrail when sending emails.
 */
class ActiveTrailMailSystem implements MailSystemInterface {

  /**
   * Concatenate and wrap the email body for either plain-text or HTML emails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    if (is_array($message['body'])) {
      $message['body'] = implode("\n\n", $message['body']);
    }
    return $message;
  }

  /**
   * Send the email message.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @see drupal_mail()
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {

    // Enable to pass in a template Id and template params (requires activetrail_template module enabled).
    if (module_exists('activetrail_template')) {
      $message['params']['template_id'] = isset($message['params']['template_id']) ? $message['params']['template_id'] : '';
      $message['params']['template_params'] = isset($message['params']['template_params']) ? $message['params']['template_params'] : [];
      drupal_alter('activetrail_template_params', $message, $message['params']['template_id'], $message['params']['template_params']);
    }

    // Test mode - relay all mails to test mail
    _activetrail_handle_test_mail_send($message);

    $activetrail_message = array(
      'toemails' => $message['to'], //comma delimited list of recipients.
      'subject' => $message['subject'] ?: $message['params']['subject'],
      'body' => $message['body'] ?: $message['params']['body'],
      'original_message' => $message,
      'campaign_name' => isset($message['params']['campaign_name']) ? $message['params']['campaign_name'] : NULL,
    );

    // Allow other modules to alter the ActiveTrail message, and sender/args.
    $activetrail_params = array(
      'message' => $activetrail_message,
      'function' => 'activetrail_sender_plain',
      'args' => array(),
    );

    // Enable modules to alter all ActiveTrail and message data
    drupal_alter('activetrail_mail', $activetrail_params, $message);

    //Send the mail.
    return activetrail_mailsend(
      $activetrail_params['message'],
      $activetrail_params['function'],
      $activetrail_params['args']
    );
  }

}

/**
 * Helper function to handle messages when test mode is enabled
 *
 * @param $message
 */
function _activetrail_handle_test_mail_send(&$message) {
  $is_test_mode = variable_get('activetrail_test_mode_enabled', FALSE);
  $test_email = variable_get('activetrail_test_email', FALSE);

  if ($is_test_mode && !empty($test_email)) {
    $original_to = $message['to'];
    $message['to'] = $test_email;

    watchdog(
      ACTIVETRAIL_WATCHDOG,
      'Test mode is enabled. Email relayed to @test_email, instead of the original to address(es): @original_to.',
      [
        '@test_email' => $test_email,
        '@original_to' => $original_to
      ],
      WATCHDOG_DEBUG
    );
  }
}
