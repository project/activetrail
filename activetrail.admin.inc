<?php

/**
 * @file
 * Administrative forms for ActiveTrail module.
 */

/**
 * Administrative settings.
 *
 * @param array $form
 *   Form render array.
 * @param array $form_state
 *   Array containing form state values.
 *
 * @return array
 *   An array containing form items to place on the module settings page.
 */
function activetrail_admin_settings($form, &$form_state) {

  $form['activetrail_api_key'] = array(
    '#title' => t('ActiveTrail API Key'),
    '#type' => 'textfield',
    '#size' => 120,
    '#description' => t('Create or grab your API key from the !link.',
      array('!link' => l(t('ActiveTrail settings'), 'https://app.activetrail.com/Members/Settings/ApiApps.aspx'))),
    '#default_value' => variable_get('activetrail_api_key'),
  );

  $form['activetrail_gropus'] = array(
    '#title' => t('ActiveTrail Groups'),
    '#type' => 'fieldset',
  );

  $form['activetrail_gropus']['activetrail_gropus_default_group'] = array(
    '#title' => t('Default group ID for new contacts'),
    '#type' => 'textfield',
    '#description' => t('All new contacts will be assigned with this group.'),
    '#default_value' => variable_get('activetrail_gropus_default_group')
  );

  $form['activetrail_test_mode_enabled'] = array(
    '#title' => t('Enable debug mode'),
    '#type' => 'checkbox',
    '#description' => t('Relay all email messages to your test email.'),
    '#default_value' => variable_get('activetrail_test_mode_enabled'),
  );

  $form['activetrail_test_mode'] = array(
    '#title' => t('Relay all emails to this address:'),
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="activetrail_test_mode_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['activetrail_test_mode']['activetrail_test_email'] = array(
    '#title' => t('Test email address'),
    '#type' => 'textfield',
    '#description' => t('All system emails will be relayed to this address.'),
    '#default_value' => variable_get('activetrail_test_email'),
  );

  $form['activetrail_test_mode']['activetrail_test_phone'] = array(
    '#title' => t('Test phone number'),
    '#type' => 'textfield',
    '#description' => t('All system sms messages will be relayed to this phone number.'),
    '#default_value' => variable_get('activetrail_test_phone'),
  );

  $form['activetrail_test_mode']['activetrail_relay_sms_to_log'] = array(
    '#title' => t('Relay SMS to watchdog'),
    '#type' => 'checkbox',
    '#description' => t('All sms messages will be relayed to the watchdog (will not be sent by real SMS).'),
    '#default_value' => variable_get('activetrail_relay_sms_to_log'),
  );

  return system_settings_form($form);
}

/**
 * Return a form for sending a test email.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function activetrail_test_form($form, &$form_state) {
  drupal_set_title(t('Send test email'));

  $form['activetrail_test_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address to send a test email to'),
    '#default_value' => variable_get('site_mail', ''),
    '#description' => t('Type in an address to have a test email sent there.'),
    '#required' => TRUE,
    '#weight' => -9
  );

  //Add subject field.
  $form['activetrail_test_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email message subject.'),
    '#default_value' => variable_get('activetrail_test_subject', t('Drupal ActiveTrail test email')),
    '#description' => t('Enter the subject for the email to be sent.'),
    '#weight' => 0,
    '#required' => FALSE,
  );

  $form['activetrail_test_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Email body'),
    '#default_value' => t('If you receive this message, it means your site is capable of using ActiveTrail to send emails.')
  );

  $formats = filter_formats();
  $options = array('' => t('-- Select --'));
  foreach ($formats as $v => $format) {
    $options[$v] = $format->name;
  }
  $form['email_options']['activetrail_filter_format'] = array(
    '#type' => 'select',
    '#title' => t('Input format'),
    '#description' => t('If selected, the input format to apply to the message body before sending to the ActiveTrail API.'),
    '#options' => $options,
    '#default_value' => array(variable_get('activetrail_filter_format', 'full_html')),
  );

  $form['test_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send test email'),
  );
  $form['test_cancel'] = array(
    '#type' => 'link',
    '#href' => 'admin/config/services/activetrail',
    '#title' => t('Cancel'),
  );

  return $form;
}

/**
 * Submit handler for activetrail_test_form(), sends the test email.
 */
function activetrail_test_form_submit($form, &$form_state) {

  global $language;

  // If an address was given, send a test email message.
  $test_address = $form_state['values']['activetrail_test_address'];

  // Set test email subject and body
  $params['subject'] = $form_state['values']['activetrail_test_subject'];
  $params['body'] = $form_state['values']['activetrail_test_body'];

  if (module_exists('activetrail_template')) {
    $params['template_id'] = $form_state['values']['activetrail_template_campaign_template'];
    $params['template_params'] = (array)json_decode($form_state['values']['activetrail_template_campaign_template_params']);
  }

  // Check for empty mailsystem config for ActiveTrail:
  $mailsystem = mailsystem_get();
  $primary_config = empty($mailsystem['activetrail_mail']) ? 'activetrail' : 'activetrail_mail';
  if (empty($mailsystem[$primary_config])) {
    drupal_set_message(t('Automatically setting ActiveTrail tests to go through ActiveTrail API: ActiveTrailMailSystem was not previously configured in Mail System.'));
    mailsystem_set(array('activetrail_mail' => 'ActiveTrailMailSystem'));
  }

  // Check for wrong mailsystem config for ActiveTrail, if not empty, and issue a warning:
  elseif ($mailsystem[$primary_config] != 'ActiveTrailMailSystem') {
    drupal_set_message(
      t('Mail System is configured to send ActiveTrail Test messages through %system, not ActiveTrail. To send tests through ActiveTrail, go to !link and change the setting.',
        array(
          '%system' => $mailsystem['activetrail_mail'],
          '!link' => l(t(ACTIVETRAIL_WATCHDOG), 'admin/config/system/mailsystem'))),
      'warning');
    // Hack because we are apparently formatting the body differently than
    // default drupal messages.
    $params['body'] = array('0' => $params['body']);
  }

  $result = drupal_mail('activetrail', 'test', $test_address, $language, $params);

  if (isset($result['result']) && $result['result'] == 'true') {
    drupal_set_message(t('ActiveTrail test email sent from %from to %to.', array('%from' => $result['from'], '%to' => $result['to'])));
  }

}
