<?php

/**
 * @file
 * ActiveTrail Rules integration.
 */

/**
 * Implement hook_rules_action_info()
 *
 * Declare ActiveTrail Rules actions.
 * @todo extend these actions to support passing variables to templates and setting the template.
 */
function activetrail_rules_action_info() {

  $actions = array(
    'send_activetrail_mail' => array(
      'label' => t(
        'Sends an email with ActiveTrail. Passes in a commerce order object, if available.'
      ),
      'group' => t('ActiveTrail'),
      'parameter' => array(
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t(
            'The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822.'
          ),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'description' => t("The mail's message body."),
          'translatable' => TRUE,
        ),
        'from' => array(
          'type' => 'text',
          'label' => t('From'),
          'description' => t(
            "The mail's from address. Leave it empty to use the site-wide configured address."
          ),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t(
            'If specified, the language used for getting the mail message and subject.'
          ),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
      'base' => 'activetrail_rules_send_mail',
    ),
  );

  return $actions;
}

/**
 * Sends an email through ActiveTrail.
 *
 * @param $to
 * @param $subject
 * @param $message
 * @param null $from
 * @param $langcode
 * @param $settings
 * @param \RulesState $state
 * @param \RulesPlugin $element
 */
function activetrail_rules_send_mail($to, $subject, $message, $from, $langcode, $settings, RulesState $state, RulesPlugin $element) {

  $to = str_replace(["\r", "\n"], '', $to);
  $from = !empty($from) ? str_replace(["\r", "\n"], '', $from) : NULL;
  $commerce_order = !empty($state->variables['commerce_order']) ? $state->variables['commerce_order'] : NULL;

  $params = [
    'order' => $commerce_order,
    'subject' => $subject,
    'message' => $message,
    'langcode' => $langcode,
  ];

  // Set a unique key for this mail.
  $name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key = 'rules_action_mail_' . $name . '_' . $element->elementId();
  $languages = language_list();
  $language = isset($languages[$langcode]) ? $languages[$langcode] : language_default();

  $message = drupal_mail('rules', $key, $to, $language, $params, $from);
  if ($message['result']) {
    watchdog('rules', 'Successfully sent email to %recipient', ['%recipient' => $to]);
  }

}
