<?php

namespace Drupal\activetrail;

use ActiveTrail\Api\Contact\PostContactTriggerContainer;
use ActiveTrail\Groups\GroupsMember;

/**
 * Class GroupsMemberService.
 */
class GroupsMemberService extends ServiceBase {

  /**
   * @var \ActiveTrail\GroupsMemberInterface
   */
  protected $client;

  /**
   * @var int
   */
  protected $groupId;

  /**
   * EmailService constructor.
   *
   * Override constructor to remove curl operations.
   *
   * @throws \Drupal\activetrail\Exception\ActiveTrailBaseException
   */
  public function __construct() {
    parent::__construct(new GroupsMember($this->getApiKey()));

    // Set the default group ID
    $this->groupId = variable_get('activetrail_gropus_default_group');
  }

  /**
   * Set the group ID for new contacts
   * @param $group_id
   */
  public function setGroupId($group_id) {
    $this->groupId = $group_id;
  }

  /**
   * Create a member on a specific ActiveTrail members group.
   * @param string $email
   *
   * @return int|null The new contact's ID or null
   */
  public function createEmailGroupsMember($email) {
    $contact = new PostContactTriggerContainer();
    $contact->email = $email;
    // Pass the contact's IP
    $contact->subscribe_ip = ip_address();
    // Bring the contact back to life, even if it was "deleted"
    $contact->is_deleted = FALSE;

    $response = $this->createGroupsMember($contact, $this->groupId);

    return !empty($response->id) ? $response->id : NULL;
  }

  /**
   * @param \ActiveTrail\Api\Contact\PostContactTriggerContainer $contact
   * @param int $group_id
   *
   * @return $this
   */
  public function createGroupsMember(PostContactTriggerContainer $contact, $group_id) {
    return $this->client->createContact($contact, $group_id);
  }

}
