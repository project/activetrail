<?php

namespace Drupal\activetrail;

use ActiveTrail\Campaign\EmailCampaign;
use ActiveTrail\EmailCampaignInterface;

/**
 * Class DrupalActiveTrail.
 */
class EmailService extends ServiceBase {

  /**
   * EmailService constructor.
   *
   * Override constructor to remove curl operations.
   *
   * @throws \Drupal\activetrail\Exception\ActiveTrailBaseException
   */
  public function __construct() {
    parent::__construct(new EmailCampaign($this->getApiKey()));
  }

  /**
   * Send the email message
   *
   * @param $message
   */
  public function send($message) {

    $client = $this->client;

    // Extract to_emails from Drupal email message
    $to_emails = activetrail_get_to($message['toemails']);
    // Get the template ID if relevant
    $template_id = $this->getMessageTemplate($message);
    // Get the message content
    $content = isset($message['body']) ? $message['body'] : NULL;
    // Get the template params
    $template_params = $this->getTemplateParams($message);

    try {
      $campaign_name = $this->getCampaignName($message);
      /* @var EmailCampaign $client */
      $client->setName($campaign_name)
        ->setSubject($message['subject'])
        ->setContent($content)
        ->addContacts($to_emails);

      // If a template was provided, set email according to template details
      if ($template_id) {
        $client->setTemplate($template_id);
      }
      // Set the template params
      if ($template_params) {
        $client->setTemplateParams($template_params);
      }

      return $client->sendCampaign(); // Send the ActiveTrailCampaign
    }
    catch (\Exception $e) {
      watchdog_exception(ACTIVETRAIL_WATCHDOG, $e, $e->getMessage());
    }

  }

  /**
   * Retrieve the list of templates that are under ActiveTrail "MyTemplates".
   * @return mixed
   */
  public function getTemplatesList() {
    /* @var EmailCampaignInterface $client */
    $client = $this->client;
    return $this->getDecodedJsonResponse($client->getMyTemplates());
  }

  /**
   * Extracts the campaign name from the message
   *
   * @param $message
   * @return string
   */
  private function getCampaignName($message) {
    $campaign_name = variable_get('site_name', '') . ' - Website';
    $campaign_name = !empty($message['campaign_name']) ? $message['campaign_name'] : $campaign_name;
    $campaign_name = ($campaign_name == 'Unknown' && !empty($_POST['form_id'])) ? $_POST['form_id'] : $campaign_name;
    return $campaign_name;
  }

  /**
   * Extract the parameters passed in with the Drupal email message.
   *
   * @param $message
   * @return mixed
   */
  private function getMessageTemplate($message) {
    $params = $message['original_message']['params'];
    return isset($params['template_id']) ? $params['template_id'] : FALSE;
  }

  /**
   * Extracts the template params passed in through the message.
   * @param $message
   * @return bool
   */
  private function getTemplateParams($message) {
    $params = $message['original_message']['params'];
    return isset($params['template_params']) ? $params['template_params'] : FALSE;
  }

}
