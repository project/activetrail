<?php

namespace Drupal\activetrail;

use ActiveTrail\CampaignInterface;
use Drupal\activetrail\Exception\ActiveTrailBaseException;
use GuzzleHttp\Psr7\Response;

/**
 * Class ServiceBase
 * @package Drupal\activetrail
 */
abstract class ServiceBase {

  protected $client;

  /**
   * ServiceBase constructor.
   *
   * Override constructor to remove curl operations.
   *
   * @param \ActiveTrail\CampaignInterface|\ActiveTrail\GroupsMemberInterface $client
   * @throws \Drupal\activetrail\Exception\ActiveTrailBaseException
   */
  public function __construct($client) {
    if (!$this->getApiKey()) {
      throw new ActiveTrailBaseException('You must provide a valid ActiveTrail API key');
    }
    $this->client = $client;
  }

  /**
   * The API key for the active trail module.
   *
   * @return string|false
   *   The API key or FALSE if none is set.
   */
  protected function getApiKey() {
    return variable_get('activetrail_api_key', FALSE);
  }

  /**
   * Extracts the response body
   *
   * @param $response
   */
  protected function getDecodedJsonResponse(Response $response) {
    return json_decode($response->getBody()->getContents());
  }

  /**
   * Override _destruct() to prevent calling curl_close().
   */
  public function __destruct() {}

}
