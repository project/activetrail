<?php

namespace Drupal\activetrail;

use ActiveTrail\Api\Contact\ApiContact;
use ActiveTrail\Api\Contact\ApiContactsImport;
use ActiveTrail\Contact\ContactsImport;

/**
 * Class ContactsImportService
 *
 * @package Drupal\activetrail
 */
class ContactsImportService extends ServiceBase {

  /**
   * @var \ActiveTrail\Contact\ContactsImport
   */
  protected $client;

  public function __construct() {
    parent::__construct(new ContactsImport($this->getApiKey()));
  }

  /**
   * @param array $contacts
   * @param string $group_id
   * @param string $mailing_list
   */
  public function import(array $contacts, $group_id = NULL, $mailing_list = NULL) {
    $payload = new ApiContactsImport();
    if ($mailing_list) {
      $payload->setMailingList($mailing_list);
    }
    if ($group_id) {
      $payload->setGroup($group_id);
    }
    foreach ($contacts as $contact) {
      $payload->addContact(ApiContact::createFromArray($contact));
    }

    return $this->client->import($payload);
  }
}