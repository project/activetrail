<?php

namespace Drupal\activetrail\Exception;

/**
 * Class ActiveTrailBaseException
 * @package Drupal\activetrail\Exception
 */
class ActiveTrailBaseException extends \Exception {

}
