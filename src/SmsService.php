<?php

namespace Drupal\activetrail;

use ActiveTrail\Campaign\SmsCampaign;
use ActiveTrail\SmsCampaignInterface;

/**
 * Class SmsService
 * @package Drupal\activetrail
 */
class SmsService extends ServiceBase implements SmsCampaignInterface {

  /**
   * @var \ActiveTrail\SmsCampaignInterface
   */
  protected $client;

  /**
   * SmsService constructor.
   */
  public function __construct() {
    parent::__construct(new SmsCampaign($this->getApiKey()));
    if (variable_get('activetrail_test_mode_enabled', FALSE) && ($test_phone = variable_get('activetrail_test_phone', FALSE))) {
      $this->client->addPhones([$test_phone]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return $this->client->getDetails();
  }

  /**
   * {@inheritdoc}
   */
  public function getCampaignScheduling() {
    $this->client->getCampaignScheduling();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addPhones(array $numbers) {
    if (!variable_get('activetrail_test_mode_enabled', FALSE)) {
      $this->client->addPhones($numbers);
    }
    else {
      watchdog(
        ACTIVETRAIL_WATCHDOG,
        'The phone numbers @numbers were not added because ActiveTrails debug mode is enabled.',
        [
          '@numbers' => implode(',', array_map('check_plain', $numbers))
        ],
        WATCHDOG_DEBUG
      );
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSendNow($send_now = TRUE) {
    $this->client->setSendNow($send_now);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setScheduledDate(\DateTime $scheduled_date) {
    $this->client->setScheduledDate($scheduled_date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnsubscribeText($unsubscribe_text) {
    $this->client->setUnsubscribeText($unsubscribe_text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCanUnsubscribe($can_unsubscribe = TRUE) {
    $this->client->setCanUnsubscribe($can_unsubscribe);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->client->setName($name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setFromName($from_name) {
    $this->client->setFromName($from_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setContent($string, $args = [], $options = []) {
    $this->client->setContent(format_string($string, $args));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function sendCampaign() {

        if (!variable_get('activetrail_relay_sms_to_log', FALSE)) {
          $this->client->sendCampaign();
        }
        else {
          watchdog(
            ACTIVETRAIL_WATCHDOG,
            'SMS has been relayed to watchdog, message content: "@content".',
            [
              '@content' => $this->client->getContent()
            ],
            WATCHDOG_DEBUG
          );
        }

  }

}
