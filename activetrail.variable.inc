<?php

/**
 * @file
 * ActiveTrail variable hooks.
 */

/**
 * Implements hook_variable_group_info().
 */
function activetrail_variable_group_info() {
  $groups['activetrail'] = array(
    'title' => t('D-kit'),
    'description' => t('Settings related to ActiveTrail.'),
    'access' => 'administer activetrail',
    'path' => array('admin/config/services/activetrail'),
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function activetrail_variable_info($options) {
  $variables = array();

  $variables['activetrail_api_key'] = array(
    'title' => t('ActiveTrail API Key', array(), $options),
    'description' => t('Create or grab your API key from the !link.',
      array('!link' => l(t('ActiveTrail settings'), 'https://app.activetrail.com/Members/Settings/ApiApps.aspx')),
      $options),
    'type' => 'string',
    'group' => 'activetrail',
    'default' => variable_get('site_mail'),
  );

  $formats = filter_formats();
  $activetrail_filter_format_options = array();
  foreach ($formats as $v => $format) {
    $activetrail_filter_format_options[$v] = $format->name;
  }
  $variables['activetrail_filter_format'] = array(
    'title' => t('Input format', array(), $options),
    'description' => t('If selected, the input format to apply to the message body before sending to the ActiveTrail API.', array(), $options),
    'type' => 'select',
    'options' => $activetrail_filter_format_options,
    'group' => 'activetrail',
    'default' => 'full_html',
  );

  return $variables;
}
