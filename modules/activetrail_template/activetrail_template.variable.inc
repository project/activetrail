<?php

/**
 * @file
 * ActiveTrail template variable hooks.
 */

/**
 * Implements hook_variable_group_info().
 */
function activetrail_template_variable_group_info() {
  $groups['activetrail_template'] = array(
    'title' => t('ActiveTrail Template'),
    'description' => t('Settings related to ActiveTrail template.'),
    'access' => 'administer activetrail',
    'path' => array('admin/config/services/activetrail/template'),
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function activetrail_template_variable_info($options) {

  $variables = array();

  $variables['activetrail_template_enable'] = array(
    'title' => t('Templates integration', array(), $options),
    'description' => t('Enable ActiveTrail templates integration.'),
    'type' => 'boolean',
    'group' => 'activetrail_template',
    'default' => FALSE,
  );

  return $variables;
}
