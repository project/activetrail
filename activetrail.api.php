<?php

/**
 * @file
 * API documentation for ActiveTrail module.
 */

/**
 * Allows to pass custom parameters to ActiveTrail templates.
 *
 * @param string $message
 *   The Drupal email message. Used to identify the context and also to set parameter values.
 * @param int $template_id
 *   The ID of the template in ActiveTrail.
 * @param array $template_params
 *   An associative array of parameters to be sent to the template.
 *   For each parameter passed, its corresponding placeholder will be replaced in the ActiveTrail template.
 *   For instance in the example below: [my_custom_var] would be replaced with: My Custom Value.
 */
function hook_activetrail_template_params_alter(&$message, &$template_id, &$template_params) {

  switch ($message['id']) {
    case 'activetrail_test':
      $template_id = 60251; // More often you'd use something like: variable_get('my_custom_email_template_id');
      $template_params = [
        'my_custom_var' => 'My Custom Value',
      ];
      break;
  }

}
